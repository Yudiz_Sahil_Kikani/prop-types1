import './App.css';
import About from './About'

function App() {
  return (
    <div className="App">
      <h2 style={{ display: 'flex', justifyContent: 'center' }}>Type Checking With Prop-Types</h2>
      <About name="sahil" Age={20} isBoolean skills={["Html",  "css",  "Bootstrap" ]}/>
    </div>
  );
}

export default App;
